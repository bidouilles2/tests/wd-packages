#!/bin/bash
#!
#! Description:
#!  Build a package binaries.
#!
#! Usage:
#!  ./build.sh <package>
#!
#! Parameters:
#!  package: a WD package e.g. seafile

set -e

# Load logger
libs/logger.sh

check_prerequisites() {
    # Parameters
    PACKAGE=$1

    # Check if parameters are provided
    if [ -z "$PACKAGE" ]; then
        logger "ERROR" "Error: package is required."
        logger "ERROR" "Usage: $0 <package>"
        exit 1
    fi

    # Check if the package exists
    if [ ! -d "packages/$PACKAGE" ]; then
        logger "ERROR" "Error: package $PACKAGE does not exist in the folder 'packages'."
        exit 1
    fi
}

build_package() {
    # Parameters
    PACKAGE=$1

    # Build the package
    logger "INFO" "Building package $PACKAGE..."
    cd packages/$PACKAGE
    ./build.sh
    cd ../..

    logger "INFO" "Package $PACKAGE created."
}

main() {
    # Parameters
    PACKAGE=$1

    logger "INFO" "Building package ${WARN}'${PACKAGE}'${INFO}."

    # Check prerequisites
    check_prerequisites $PACKAGE

    # Build the package
    build_package $PACKAGE
}

main $@
