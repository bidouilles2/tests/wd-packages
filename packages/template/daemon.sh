#!/bin/bash

set -e

#! Constants
APP_NAME="template"
APP_VERSION="X.X.X"
APP_LOG_FILE="/tmp/pkg_${APP_NAME}_${APP_VERSION}.log"
APP_LOG="[APP][$0] $(date +'%Y-%m-%d %H:%M:%S') |"

APP_ROOT_PATH=/shares/Volume_1/Nas_Prog/


is_mountpoint () {
    mnts=$(cat /proc/self/mounts | grep "$1" | awk '{print $2}')

    for i in "${mnts[@]}"; do
        if [ "$i" == "$1" ]; then
            return 0
        fi
    done

    return 1
}

is_setup () {
    if [ true ]; then
        echo "$APP_LOG Setup Success!"
    else
        echo "$APP_LOG Setup failed!"
        return 1
    fi
}

setup () {
    echo "$APP_LOG Setting up app"

    echo "$APP_LOG Setup complete!"
}

function stop {
    echo "$APP_LOG App is stopped."
}

function dm_cleanup {
    SHM_MOUNTS=$(cat /proc/self/mounts | grep "shm.*${APP_NAME}" | awk '{print $2}')

    # if SHM_MOUNTS is not empty, unmount them
    if [ ! -z "${SHM_MOUNTS}" ]; then
        echo "$APP_LOG Unmounting SHM mounts: ${SHM_MOUNTS}"
        for MOUNT_POINT in "${SHM_MOUNTS[@]}"; do
            echo "$APP_LOG shm_cleanup: ${MOUNT_POINT}"
            umount "$MOUNT_POINT"
        done
    fi

    if [ ! -z $(cat /proc/self/mounts | grep -c ${APP_NAME}) ]; then
        echo "$APP_LOG Unmount all /var/lib/... of the App."
    fi

    dmsetup remove_all
}

function cleanup {
    echo "$APP_LOG Cleaning up."

    # remove cgroup stuff
    /usr/sbin/cgroupfs-umount

    dm_cleanup
}

function set_cgroup {
    ONE_G
    _KB=1048576

    mem_quota=0
    mem_total_kb=$(grep MemTotal /proc/meminfo 2>/dev/null | awk '{print $2}')

    if [[ ! "${mem_total_kb}" =~ ^[0-9]+$ ]]; then
        echo "$APP_LOG Failed to get total memory!"
        return 1
    fi

    if [ ${mem_total_kb} -gt ${ONE_G_KB} ]; then
        mem_quota=$((mem_total_kb / 2))
    else
        mem_quota=$((mem_total_kb / 3))
    fi

    echo "$APP_LOG Total RAM: ${mem_total_kb} KB"

    if is_mountpoint /sys/fs/cgroup/memory; then
        echo "$APP_LOG Creating /sys/fs/cgroup/memory/$APP_NAME"
        mkdir -p /sys/fs/cgroup/memory/$APP_NAME
    else
        echo "$APP_LOG /sys/fs/cgroup/memory is not a cgroup mount"
        return 1
    fi

    echo "$APP_LOG quota: ${mem_quota} KB"
    if echo "${mem_quota}K" >/sys/fs/cgroup/memory/$APP_NAME/memory.limit_in_bytes; then
        echo 1 >/sys/fs/cgroup/memory/$APP_NAME/memory.use_hierarchy
        echo -n "Set memory quota for $APP_NAME: "
        cat /sys/fs/cgroup/memory/$APP_NAME/memory.limit_in_bytes
    fi
}

main() {
    case $1 in
    start)
        echo "$APP_LOG Starting $APP_NAME"

        is_setup || (echo "$APP_LOG  $APP_NAME is not setup! Run daemon.sh setup" && exit 1)
        dm_cleanup
        cgroupfs-mount
        set_cgroup
        ;;
    stop)
        echo "$APP_LOG Stopping $APP_NAME"

        stop
        ;;
    status)
        echo "$APP_LOG Checking $APP_NAME status"
        ;;
    setup)
        echo "$APP_LOG Setting up $APP_NAME"

        setup
        ;;
    issetup)
        echo "$APP_LOG Checking if $APP_NAME is setup"

        if ! is_setup; then
            exit 1
        fi
        ;;
    shutdown)
        echo "$APP_LOG Shutting down $APP_NAME"

        stop
        cleanup
        ;;
    *)
        echo "$APP_LOG Invalid command!"
        exit 1
        ;;
    esac

    echo "$APP_LOG Done!"
}

main "$@" >>$APP_LOG_FILE 2>&1
