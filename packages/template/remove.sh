#!/bin/sh
#!
#! Description:
#!  Remove Docker App
#!
#! Usage:
#!  ./remove.sh

set -e

#! Constants
APP_NAME="docker"
APP_VERSION="26.0.2"
LOG_FILE="/tmp/debug_apkg"

#! COLORS
NOCOLOR='\033[0m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'
DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'

logger() {
    # Parameters
    MESSAGE=$1

    # Variables
    FILE_NAME=$(basename $0)

    echo -e "${ORANGE}[APP] ${APP_NAME}(v${APP_VERSION}) | ${CYAN}$(date +'%Y-%m-%d %H:%M:%S') ${YELLOW}[${FILE_NAME}]${NOCOLOR} ${MESSAGE}"
}

main() {
    [ -f /tmp/debug_apkg ] && logger "APKG_DEBUG: $0 $@"

    logger "Restore original docker if exists"
    [ -f /usr/sbin/docker.bak ] && mv -f /usr/sbin/docker.bak /usr/sbin/docker

    logger "Remove bins"
    rm -rf /sbin/docker*
    rm -rf /sbin/containerd
    rm -rf /sbin/containerd-shim
    rm -rf /sbin/containerd-shim-runc-v2
    rm -rf /sbin/ctr
    rm -rf /sbin/runc

    logger "Remove mountpoint"
    rm -rf /var/lib/docker

    logger "Remove web"
    rm -rf /var/www/docker

    logger "${GREEN}$(basename $0) DONE!${NOCOLOR}"
}

main "$@" >>$LOG_FILE 2>&1
