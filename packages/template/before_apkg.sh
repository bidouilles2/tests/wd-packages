#!/bin/bash

set -e

#! Constants
APP_NAME="template"
APP_VERSION="X.X.X"
APP_LOG_FILE="/tmp/pkg_${APP_NAME}_${APP_VERSION}.log"
APP_LOG="[APP][$0] $(date +'%Y-%m-%d %H:%M:%S') |"

main() {
    # DO NOT REMOVE
    # I don't know if it's required for backward compatibility
    # I don't have time to figure it out
    APKG_MODULE="docker"
    APKG_PATH=""

    echo "$APP_LOG Before APKG Done."
}

main "$@" >>$APP_LOG_FILE 2>&1
