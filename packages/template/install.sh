#!/bin/bash

set -e

#! Constants
APP_NAME="template"
APP_VERSION="X.X.X"
APP_LOG_FILE="/tmp/pkg_${APP_NAME}_${APP_VERSION}.log"
APP_LOG="[APP][$0] $(date +'%Y-%m-%d %H:%M:%S') |"

prerequisites() {
    # Parameters
    PATH_SRC=$1
    NAS_PROG=$2

    # Check if the debug file exists
    [ -f $LOG_FILE ] && echo "$APP_LOG APKG_DEBUG: $0 $@"

    # Move the source code to the NAS_PROG
    echo "$APP_LOG Install all package scripts to the proper location ($PATH_SRC > $NAS_PROG)"
    cp -rf $PATH_SRC $NAS_PROG
}

stop_daemon() {
    if [ -e "${ORIG_DAEMONSH}" ]; then
        echo "$APP_LOG Found orig daemon: ${ORIG_DAEMONSH}"
        /usr/sbin/${APP_NAME}_daemon.sh shutdown
        sleep 1
        mv /usr/sbin/${APP_NAME}_daemon.sh /usr/sbin/${APP_NAME}_daemon.sh.bak
    else
        echo "$APP_LOG No orig daemon found"
    fi
}

setup_root() {
    # Parameters
    NAS_PROG=$1

    # Variables
    DROOT=${NAS_PROG}/_${APP_NAME}

    if [ -d ${DROOT} ]; then
        if [ -d ${DROOT}/devicemapper ]; then
            echo "$APP_LOG Found old ${APP_NAME} devicemapper storage.. backup and create new ${APP_NAME} root"
            mv "${DROOT}" "${DROOT}.bak"
            mkdir -p "${DROOT}"
        else
            echo "$APP_LOG Found existing ${APP_NAME} storage. Reusing."
        fi
    else
        echo "$APP_LOG Creating new ${APP_NAME} root"
        mkdir -p "${DROOT}"
    fi
}

start() {
    # Parameters
    APP_PATH=$1

    "${APP_PATH}/daemon.sh" setup
    sleep 1

    "${APP_PATH}/daemon.sh" start
    sleep 3
}

main() {
    # Parameters
    PATH_SRC=$1
    NAS_PROG=$2

    # Variables
    APP_PATH="${NAS_PROG}/${APP_NAME}"

    echo "$APP_LOG Installing ${APP_NAME} with APP_PATH: ${APP_PATH}"

    echo "$APP_LOG Prerequisites"
    prerequisites $PATH_SRC $NAS_PROG

    echo "$APP_LOG Stop original ${APP_NAME} daemon"
    stop_daemon

    echo "$APP_LOG Setup ${APP_NAME} binaries in PATH so they are found before the 1.7 binaries"
    ln -s ${APP_PATH}/${APP_NAME}/* /sbin

    echo "$APP_LOG Setup persistent ${APP_NAME} root directory"
    setup_root $NAS_PROG

    echo "$APP_LOG Starting ${APP_NAME}"
    start $APP_PATH

    echo "$APP_LOG Done!"
}

main "$@" >>$APP_LOG_FILE 2>&1
