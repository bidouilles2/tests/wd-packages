#!/bin/bash

set -e

#! Constants
APP_NAME="template"
APP_VERSION="X.X.X"
APP_LOG_FILE="/tmp/pkg_${APP_NAME}_${APP_VERSION}.log"
APP_LOG="[APP][$0] $(date +'%Y-%m-%d %H:%M:%S') |"

main() {
    echo "$APP_LOG Execution: $0 $@"

    echo "$APP_LOG Remove web"
    WEBPATH="/var/www/$APP_NAME"
    rm -rf $WEBPATH

    echo "$APP_LOG Remove symlinks"

    echo "$APP_LOG Done!"
}

main "$@" >>$APP_LOG_FILE 2>&1
