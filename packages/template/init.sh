#!/bin/bash

set -e

#! Constants
APP_NAME="template"
APP_VERSION="X.X.X"
APP_LOG_FILE="/tmp/pkg_${APP_NAME}_${APP_VERSION}.log"
APP_LOG="[APP][$0] $(date +'%Y-%m-%d %H:%M:%S') |"

main() {
    APPDIR=$1

    echo "$APP_LOG INIT linking files from path: $path"

    # setup binaries in PATH before the original v1.7 binaries
    echo "$APP_LOG Setup binaries in PATH before the original v1.7 binaries"
    echo "$APP_LOG TODO: Remove this useless step..."
    ln -s $(readlink -f ${APPDIR})/${APP_NAME}/* /sbin || true

    # disable default App by moving the original start script
    echo "$APP_LOG Disable default ${APP_NAME} by moving the original start script"
    [ -L /usr/sbin/${APP_NAME}_daemon.sh ] && mv /usr/sbin/${APP_NAME}_daemon.sh /usr/sbin/${APP_NAME}_daemon.sh.bak
    [ -L /usr/sbin/${APP_NAME} ] && mv /usr/sbin/${APP_NAME} /usr/sbin/${APP_NAME}.bak

    # create folder for the redirecting webpage
    echo "$APP_LOG Create folder for the redirecting webpage"
    WEBPATH="/var/www/${APP_NAME}/"
    mkdir -p ${WEBPATH}
    ln -sf ${APPDIR}/web/* $WEBPATH

    echo "$APP_LOG Done!"
}

main "$@" >>$APP_LOG_FILE 2>&1
