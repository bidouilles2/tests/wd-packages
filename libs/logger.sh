#!/bin/bash

# Colors
NOCOLOR='\033[0m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'
DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'

logger() {
    # Parameters
    STATUS="$1"
    MESSAGE="$2"

    # Get colors from status
    case $STATUS in
    "ERROR") STAT_COLOR=$RED ;;
    "DEBUG") STAT_COLOR=$CYAN ;;
    "INFO") STAT_COLOR=$GREEN ;;
    "WARN") STAT_COLOR=$ORANGE ;;
    *) STAT_COLOR=$NOCOLOR ;;
    esac

    if [ -z "$MESSAGE" ]; then
        echo -e "$(date +'%Y-%m-%d %H:%M:%S') ${RED}[ERROR]${NOCOLOR} logger.sh - No message provided."
        exit 1
    fi

    echo -e "$(date +'%Y-%m-%d %H:%M:%S') ${STAT_COLOR}[${STATUS}]${NOCOLOR} ${MESSAGE}"
}
